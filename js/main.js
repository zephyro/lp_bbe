$(".btn-modal").fancybox({
    'padding'    : 0
});


$(".project-item").hover(
    function(){

        $(this).closest(".project-group").addClass("hover");

    },function(){

        $(this).closest(".project-group").removeClass("hover");
    }
);

// Tabs

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});



$('.project-item').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.project');


    box.find('.project-content-item').removeClass('active');
    box.find(tab).addClass('active');
    box.toggleClass('open');
});


$('.btn-back').click(function(e) {
    e.preventDefault();
    $(this).closest('.project-group').toggleClass('open');
});

$('.btn-back').click(function(e) {
    e.preventDefault();
    $('.project').toggleClass('open');
});


$(document).ready(function(){

    $('.btn-send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form = $(this).closest('form'),
                type    =     $('input[name="type"]', $form).val(),
                name    =     $('input[name="name"]', $form).val(),
                phone   =     $('input[name="phone"]', $form).val(),
                email   =     $('input[name="email"]', $form).val();
            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {name: name, phone: phone, type:type, email: email}
            }).done(function(msg) {
                $('form').find('input[type=text], textarea').val('');
                console.log('удачно');
                document.location.href = "./done.html";
            });
        }
    });
});
